/*
  MIT License

  Copyright (c) 2022 Open Source  UOM

  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:

  The above copyright notice and this permission notice shall be included in all
  copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
  SOFTWARE.

  Made by Open Source UoM (https://opensource.uom.gr)

  Project members:
    -Apostolidis
    -Davios
    -Iosifidis
    -Konstantinidis
    -Mpakalis
    -Nasis
    -Omiliades
    -Patsouras
    -Fakidis

*/

export const servicesData = [
  {
    title: "Γραφείο Διασύνδεσης",
    url: "https://www.uom.gr/career-office",
    category: "Γραφείο",
    imgUrl:
      "https://www.uom.gr/assets/site/public/nodes/11869/13503-GrafeioDiasyndesis-2.jpg",
  },
  {
    title: "Γραφείο Πρακτικής",
    url: "http://practice.uom.gr/",
    category: "Γραφείο",
    imgUrl:
      "https://www.uom.gr/assets/site/public/nodes/8621/7799-praktikiaskisikyriafoto-4.jpg",
  },
  {
    title: "Γραφείο Αποφοίτων",
    url: "https://www.uom.gr/apofitoi",
    category: "Γραφείο",
    imgUrl: "https://www.uom.gr/assets/site/content/alumni/ALUMNI_LOGO_GR.PNG",
  },
  {
    title: "Γραφείο Erasmus",
    url: "https://www.uom.gr/erasmus-office",
    category: "Γραφείο",
    imgUrl:
      "https://www.uom.gr/assets/site/public/nodes/4228/2644-erasmus-header-2.jpg",
  },
  {
    title: "Γραφείο Φυσικής Αγωγής",
    url: "https://www.uom.gr/gym-office",
    category: "Γραφείο",
    imgUrl: "https://pbs.twimg.com/media/C5WlRPKWcAIJ90T.jpg",
  },
  {
    title: "Σύλλογος Φοιτητών Erasmus",
    url: "http://afroditi.uom.gr/erasmusp",
    category: "Σύλλογος",
    imgUrl: "https://opensource.uom.gr/myuom_images/esnuom.png",
  },
  {
    title: "Σύλλογος Φοιτητών ΠΑΜΑΚ",
    url: "https://www.facebook.com/sfpamak/",
    category: "Σύλλογος",
    imgUrl:
      "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcSiD8KP64gxKFvyW4kbF-NK5Ao--9oSq9Fziw&usqp=CAU",
  },
  {
    title: "TedxUniversityofMacedonia",
    url: "https://tedxuniversityofmacedonia.com/",
    category: "Σύλλογος",
    imgUrl:
      "https://p77-sign-va.tiktokcdn.com/tos-maliva-avt-0068/b9772e1f6046e2a400bd049d39e2b30f~c5_100x100.jpeg?x-expires=1662890400&x-signature=RL58sc0MrQ%2Frls37bxGYeUbkaGk%3D",
  },
  {
    title: "AIESEC",
    url: "https://www.uom.gr/aiesec",
    category: "Σύλλογος",
    imgUrl:
      "https://upload.wikimedia.org/wikipedia/commons/4/4a/AIESEC-New-Logo1.png",
  },
  {
    title: "Finance Club",
    url: "https://www.financeclubuom.org/",
    category: "Σύλλογος",
    imgUrl:
      "https://yt3.ggpht.com/ytc/AKedOLQ86pDFLPNZTJulIeWRCJajirV5WYsCiapFivo0=s900-c-k-c0x00ffffff-no-rj",
  },
  {
    title:
      "Άλικο ΚΘΒΕ-ΠΑΜΑΚ (πρώην Πολυφωνικά Αναλόγια) & Θεατρικό Εργαστήρι ΚΘΒΕ-ΠΑΜΑΚ",
    url: "https://www.uom.gr/aliko-kthbe-pamak-prohn-polyfonika-analogia-theatriko-ergasthri-kthbe-pamak",
    category: "Σύλλογος",
    imgUrl:
      "https://www.uom.gr/assets/site/public/nodes/6799/4531-aliko-theatrikoergastiri-kthvepamak-4.jpg",
  },
  {
    title: "Open Source UoM",
    url: "https://opensourceuom.gitlab.io/",
    category: "Σύλλογος",
    imgUrl: "https://opensourceuom.gitlab.io/assets/img/team/avatar.png",
  },
  {
    title: "Λέσχη Κυβερνοασφάλειας Πανεπιστημίου Μακεδονίας",
    url: "https://csc.uom.gr/",
    category: "Σύλλογος",
    imgUrl: "https://csc.uom.gr/wp-content/uploads/2021/02/Myrmidones_02.jpg",
  },
  {
    title: "Γραφείο Περιβαλλοντικής Διαχείρισης ΠαΜακ ",
    url: "http://www.perivpamak.gr/",
    category: "Γραφείο",
    imgUrl:
      "https://1.bp.blogspot.com/-t4PMium6v6g/X2x6Sz0aAFI/AAAAAAAAD4A/bJNjlS_kYr4lXGQdQBAD4mMdmrWEJ0qjgCNcBGAsYHQ/s2048/Logo%2B%25CE%25A0%25CE%25B5%25CF%2581%25CE%25B9%25CE%25B2%25CE%25B1%25CE%25BB%25CE%25BB%25CE%25BF%25CE%25BD%25CF%2584%25CE%25B9%25CE%25BA%25CE%25AE%25CF%2582.png",
  },
  {
    title:
      "Επιτροπή Ηθικής και Δεοντολογίας της Έρευνας (Ε.Η.Δ.Ε.) του Πανεπιστημίου Μακεδονίας",
    url: "https://www.uom.gr/ethics",
    category: "Υπόλοιπες",
    imgUrl: "https://www.uom.gr/assets/site/public/nodes/4013/10348-7.png",
  },
  {
    title: "Συνήγορος του Φοιτητή",
    url: "https://www.uom.gr/synhgoros-toy-foithth-2021-2022",
    category: "Υπόλοιπες",
    imgUrl:
      "https://foititisonline.gr/wp-content/uploads/2018/08/sinigoros-tou-foititi-3.jpg",
  },
  {
    title: "Επιτροπή Ισότητας Φύλων",
    url: "https://www.uom.gr/eif",
    category: "Υπόλοιπες",
    imgUrl: "https://www.uom.gr/assets/site/public/nodes/8310/9033-EIFLOGO.jpg",
  },
  {
    title: "Κέντρο Συμβουλευτικής και Στήριξης Φοιτητών",
    url: "https://www.uom.gr/student-support",
    category: "Υπόλοιπες",
    imgUrl: "https://opensource.uom.gr/myuom_images/kssf.jpeg",
  },
  {
    title: "Μονάδα Προσβασιμότητας ΠΑΜΑΚ",
    url: "https://www.uom.gr/accessibility",
    category: "Υπόλοιπες",
    imgUrl:
      "https://scontent.fskg1-2.fna.fbcdn.net/v/t1.6435-9/162199791_193459489248515_2795892316391728468_n.jpg?_nc_cat=107&ccb=1-7&_nc_sid=09cbfe&_nc_ohc=IluJohFv1-8AX9OpJ8G&_nc_ht=scontent.fskg1-2.fna&oh=00_AT8_DZhdnm_-2p642TAReo8lDCKoUQupcujQ0FhiBmKgRg&oe=632D228D",
  },
  {
    title: "Εκδόσεις Πανεπιστημίου Μακεδονίας",
    url: "https://www.uompress.gr/index.php/el/#",
    category: "Υπόλοιπες",
    imgUrl: "https://www.uompress.gr/images/stories/logo_uom_v.png",
  },
];
